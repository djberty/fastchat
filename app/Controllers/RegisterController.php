<?php

namespace App\Controller;

use App\Models\User;
use Fast\Controller\Controller;
use Fast\Controller\Exception;
use Fast\Model\MySQLDate;

class RegisterController extends Controller
{
    protected $valid_methods = [
        'post'
    ];

    public function post()
    {
        $post = $this->getApp()->getRequest()->post();
        $username = $post->get('username');

        /** @var User $user */
        $user = $this->getModel('User');

        if ($user->userExistsByUsername($username)) {
            throw new Exception('Cannot register user, username already exists: ' . $username, 500);
        }

        $email = $post->get('email');
        $password = $post->get('password');
        $password_verify = $post->get('password_verify');

        if ($password != $password_verify) {
            throw new Exception('Cannot register user, passwords do not match.', 500);
        }

        $salt = $this->getAuth()->generateSalt();
        $password = $this->getAuth()->hashPassword($password, $salt);

        $user->setField('username', $username);
        $user->setField('email', $email);
        $user->setField('password', $password);
        $user->setField('salt', $salt);
        $user->setField('datetime_created', new MySQLDate());
        $user->save();
    }
} 