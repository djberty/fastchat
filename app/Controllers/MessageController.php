<?php

namespace App\Controller;

use App\Model\Message;
use Fast\Controller\Controller;
use Fast\Model\MySQLDate;

class MessageController extends Controller
{
    protected $valid_methods = [
        'get', 'post'
    ];

    public function requiresAuthentication()
    {
        return true;
    }

    public function get()
    {
        \Fast\App::debug(false);

        //$user_id = (int) $this->getApp()->getRequest()->get()->get('user_id');
        $user_id = 1; // @todo
        $count = (int) $this->getApp()->getRequest()->get()->get('count');
        if (!$count) {
            $count = 5; // @todo config setting
        }

        /** @var Message $message_model */
        $message_model = $this->getModel('Message');
        $messages = $message_model->getForUser($user_id, $count);

        // convert to arrays for JSON
        foreach ($messages as $key => $message) {

            $messages[$key] = $message->toArray();

            // convert dates to UK? format
            if ($messages[$key]['date_created']) {
                $messages[$key]['date_created'] = date('d/m/Y H:i:s', strtotime($messages[$key]['date_created']));
            }
            if ($messages[$key]['date_modified']) {
                $messages[$key]['date_modified'] = date('d/m/Y H:i:s', strtotime($messages[$key]['date_modified']));
            }
        }

        $this->getApp()->getResponse()->setHeader('Content-Type', 'application/json');

        return json_encode($messages);
    }

    public function post()
    {
        $message = trim($this->getApp()->getRequest()->post()->get('message'));

        if ($message) {
            $from_user_id = 1; // @todo - get from somewhere internal (after auth?)
            $to_user_id = 1; // @todo

            $message_model = $this->getModel('Message');
            $message_model->setField('message_text', $message);
            $message_model->setField('from_user_id', $from_user_id);
            $message_model->setField('to_user_id', $to_user_id);
            $message_model->setField('date_created', new MySQLDate());

            $error = '';
            $success = $message_model->save();
            if (!$success) {
                $error = 'Unable to save posted message';
                $data = ['success' => $success, 'error' => $error];
            } else {
                $data = $message_model->toArray();
            }
        } else {
            $error = 'No message posted';
            $data = ['success' => false, 'error' => $error];
        }

        return json_encode($data);
    }
}
