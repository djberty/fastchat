<?php

namespace App\Controller;

use App\Model\Message;
use Fast\Controller\Controller;

class MessageReadController extends Controller
{
    protected $valid_methods = [
        'post'
    ];

    public function requiresAuthentication()
    {
        return true;
    }

    public function post()
    {
        $message_ids = $this->getApp()->getRequest()->post()->get('message_ids');
        $user_id = 1; // @todo

        /** @var Message $message_model */
        $message_model = $this->getModel('Message');

        $successes = [];
        foreach (explode(',', $message_ids) as $message_id) {
            $successes[] = $message_model->markMessageRead($message_id, $user_id);
        }

        return count($successes);
    }
} 