<?php

namespace App\Controller;

use Fast\Controller\Controller;

class LogoutController extends Controller
{
    public function get()
    {
        $this->getAuth()->unauthenticate();
        return "1";
    }
} 