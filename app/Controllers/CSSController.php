<?php

namespace App\Controller;

use Fast\Controller\Controller;

/**
 * Class CSSController
 * Captures requests for CSS files and attempts to process and return less files with the same name
 * (excluding the file extension which remains as .css)
 * @package App\Controller
 */
class CSSController extends Controller
{
    public function get()
    {
        $name = $this->getApp()->getRequest()->get()->get('name');
        $less_path = FAST_ROOT . 'app/Resources/less/' . $name . '.less';

        if (is_file($less_path)) {
            require FAST_ROOT . 'vendors/LessPHP/lessc.inc.php';
            $less = file_get_contents($less_path);
            $this->getApp()->getResponse()->setHeader('Content-Type', 'text/css');
            $less_processor = new \lessc();
            return $less_processor->compile($less);

        } else {
            $this->getApp()->getResponse()->setStatus(404);
            return 'CSS Not Found';
        }
    }
} 