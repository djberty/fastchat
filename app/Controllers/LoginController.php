<?php

namespace App\Controller;

use App\Models\Auth;
use App\Models\User;
use Fast\Controller\Controller;
use Fast\Controller\Exception;

class LoginController extends Controller
{
    protected $valid_methods = [
        'post'
    ];

    public function post()
    {
        $username = $this->getApp()->getRequest()->post()->get('username');
        $password = $this->getApp()->getRequest()->post()->get('password');

        /** @var User $user */
        $user = $this->getModel('User');
        $user->populateByUsername($username);

        if ($user->getId()) {

            /** @var Auth $auth_model */
            $auth_model = $this->getModel('Auth');
            $auth_model->setUser($user);
            $this->getAuth()->setAuthModel($auth_model);

            $success = $this->getAuth()->authenticate($password);

            if (!$success) {
                throw new Exception('Login Failure - Unknown password', 403); // @todo remove specific errors
            } else {
                return '1'; // success
            }
        } else {
            throw new Exception('Login Failure - Unknown username', 403); // @todo remove specific errors
        }
    }
} 