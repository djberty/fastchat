<?php

namespace App\Controller;

use Fast\Autoloader\Autoloader;
use Fast\Controller\Controller;
use Fast\Controller\Exception;

class TemplateController extends Controller
{
    public function get()
    {
        $name = basename($this->getApp()->getRequest()->get()->get('name'));
        $template = Autoloader::instance()->loadResource('js-templates/' . $name . '._js');

        if ($template !== null) {
            $this->getApp()->getResponse()->setHeader('Content-Type', 'text/plain');
            return $template;
        } else {
            throw new Exception("Requested template does not exist: " . $name);
        }
    }
} 