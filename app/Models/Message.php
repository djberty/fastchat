<?php

namespace App\Models;

use Fast\Model\MySQL;

class Message extends MySQL
{
    protected $table = 'message';

    protected $primary_key = 'message_id';

    protected $fields = [
        'message_id',
        'from_user_id',
        'to_user_id',
        'message_text',
        'date_created',
        'date_modified'
    ];

    /**
     * @param int $user_id
     * @param int $count
     * @return Message[]
     */
    public function getForUser($user_id, $count)
    {
        $messages = [];

        // Also get messages where to_user_id = 0  because they are to everyone in chat

        $sql = 'SELECT m.*
                FROM message AS m
                LEFT JOIN message_read AS mr ON (m.to_user_id = mr.user_id AND m.message_id = mr.message_id)
                WHERE (m.to_user_id = :user_id OR m.to_user_id = 0)
                    AND mr.message_id IS NULL
                ORDER BY date_created DESC
                LIMIT ' . (int) $count;

        $query = $this->getDB()->prepare($sql);

        if ($query && $query->execute([':user_id' => $user_id])) {
            $rows = $query->fetchAll();
            foreach ($rows as $row) {
                $messages[] = new Message($row);
            }
        }
        return $messages;
    }

    /**
     * Marks a message read on the message_read table for the specified user_id
     * @param $message_id
     * @param $user_id
     * @return bool
     */
    public function markMessageRead($message_id, $user_id)
    {
        $sql = 'INSERT IGNORE INTO message_read
                    (message_id, user_id)
                VALUES
                    (:message_id, :user_id)';

        $query = $this->getDB()->prepare($sql);
        if ($query) {
            return $query->execute(['message_id' => $message_id, ':user_id' => $user_id]);
        }
        return false;
    }
}