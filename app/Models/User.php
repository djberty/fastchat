<?php

namespace App\Models;

use Fast\DB\DB;
use Fast\Model\Exception;
use Fast\Model\MySQL;

class User extends MySQL
{
    protected $table = 'user';

    protected $primary_key = 'user_id';

    protected $fields = [
        'user_id',
        'username',
        'password',
        'salt',
        'email',
        'firstname',
        'lastname',
        'datetime_created'
    ];

    /**
     * @param string $username
     */
    public function populateByUsername($username)
    {
        $query = $this->getDB()->prepare('
            SELECT *
            from user
            WHERE username = :username
            LIMIT 1
        ');
        if ($query && $query->execute([':username' => $username])) {
            $row = $query->fetch();
            $this->setData($row);
        }
    }

    /**
     * @param $username
     * @return bool
     * @throws \Fast\Model\Exception
     */
    public function userExistsByUsername($username)
    {
        $query = $this->getDB()->prepare('
            SELECT COUNT(*) as user_count
            from user
            WHERE username = :username
            LIMIT 1
        ');
        if ($query->execute([':username' => $username])) {
            $row_count = $query->fetchColumn();
            return $row_count > 0;
        } else {
            throw new Exception('Cannot determine if user exists: ' . $username, 500);
        }
    }
} 