<?php

namespace App\Models;

use Fast\Auth\Credentials;
use Fast\Auth\Model;

class Auth extends Model
{
    /**
     * @var User
     */
    private $user = null;

    public function setUser(User $user)
    {
        $this->user = $user;
        if ($this->user->getField('username')) {
            $this->setUsername($this->user->getField('username'));
        }
    }

    /**
     * Populates the passed in Credentials object, must return bool indicating whether credentials were found
     * @param Credentials $credentials
     * @return bool
     */
    public function loadCredentials(Credentials $credentials)
    {
        $credentials->setUserId($this->user->getId());
        $credentials->setUsername($this->user->getField('username'));
        $credentials->setPassword($this->user->getField('password'));
        $credentials->setSalt($this->user->getField('salt'));
        return true;
    }

    /**
     * @param Credentials $credentials
     * @return bool
     */
    public function saveCredentials(Credentials $credentials)
    {
        $this->user->setField('password', $credentials->getPassword());
        $this->user->setField('salt', $credentials->getSalt());
        return $this->user->save();
    }

} 