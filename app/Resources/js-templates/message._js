<div id="msg-<%= message_id %>" class="message" data-id="<%= message_id %>">
    <div class="date"><%- date_created %></div>
    <div class="from">From: <%- from_user_id %></div>
    <div class="message-content"><%- message_text %></div>
</div>