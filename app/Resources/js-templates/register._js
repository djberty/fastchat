
<div id="register">
    <form method="post" action="/register">
        <label for="username">Username:</label>
        <input id="username" type="text" value="" name="username" />
        <label for="email">Email (not shown):</label>
        <input id="email" type="text" value="" name="email" />
        <label for="password">Password:</label>
        <input id="password" type="password" name="password" />
        <label for="password_verify">Password Again:</label>
        <input id="password_verify" type="password" name="password_verify" />
        <input type="submit" value="Register" />
    </form>
</div>