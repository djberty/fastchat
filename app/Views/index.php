<?php $this->getBlock('title')->setContent('Chat') ?>

<?php $this->getBlock('js')->start() ?>
    <script type="text/javascript" src="/js/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="/js/underscore.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript" src="/js/chat.js"></script>
<?php $this->getBlock('js')->end() ?>

<?php $this->getBlock('css')->start() ?>
    <link type="text/css" rel="stylesheet" href="/css/test.css" />
<?php $this->getBlock('css')->end() ?>

<?php $this->getBlock('meta')->start() ?>
    <!-- @todo Meta Data -->
<?php $this->getBlock('meta')->end() ?>

<?php $this->getBlock('content')->start() ?>

    <header>
        <div id="links">
            <span class="not-authenticated">
                <a id="login-link" href="#login" class="modal">Login</a> |
                <a id="register-link" href="#register" class="modal">Register</a>
            </span>
            <span class="authenticated">
                <a id="logout-link" href="#logout">Logout</a>
            </span>
        </div>
        <h1>Chat</h1>
    </header>

    <section>
        <div id="client">

            <div id="messages"></div>

            <form id="message_form" method="post" action="/messages/">
                <div id="errors"></div>
                <label for="message_text">Hit me with it...</label>
                <input type="text" name="message_text" id="message_text" autocomplete="off" />
                <input class="button" type="submit" value="Post Message" />
                <!--<input class="button" type="button" name="stop_refresh" id="stop_refresh" value="Stop Refresh" />-->
                <div class="cf"></div>
            </form>
        </div>
    </section>

    <div id="modals">

        <div id="login" class="window">
            <a href="#" class="close">X</a>
            <div class="modal_content"></div>
        </div>

        <div id="register" class="window">
            <a href="#" class="close">X</a>
            <div class="modal_content"></div>
        </div>

        <div id="mask"></div>

    </div>
<?php $this->getBlock('content')->end() ?>