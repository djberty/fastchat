
<?php $this->getBlock('content')->start() ?>

    <h1>An Error Occurred:</h1>

    <h2><?php echo $this->escape($this->exception->getMessage()) ?></h2>

    <h3>Trace:</h3>
    <p><?php echo nl2br($this->escape($this->exception->getTraceAsString())) ?></p>

<?php $this->getBlock('content')->end() ?>