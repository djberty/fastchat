<?php

define('FAST_ROOT', realpath('../') . '/');

require "../vendors/Fast/Autoloader/Autoloader.php";
\Fast\Autoloader\Autoloader::init();

if (\Fast\Config\Config::getEnvironment() == 'dev') {
    \Fast\App::debug();
}

$app = new \Fast\App();
$app->run();