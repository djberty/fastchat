"use strict";

var Chat = {
    self: this,
    refresh_timeout: 5000, // ms
    templates: {},
    received_message_ids: [],
    error_hide_timeout: 5000 // ms
};

/**
 * Initialises the chat application
 */
Chat.init = function()
{
    Chat.getMessages();
    var update_interval = setInterval(Chat.getMessages, Chat.refresh_timeout);

    $('#stop_refresh').click(function(e) {
        e.preventDefault();
        clearInterval(update_interval);
    });

    $('#message_form').submit(function(e) {
        e.preventDefault();
        Chat.postMessage($('#message_text').val());
    });

    $('#message_text').focus();

    // handle unauthorised AJAX responses
    $(document).ajaxError(function(e, request) {
        if (request.status == 403) {
            if (!$("#login").is(':visible')) {
                $('.authenticated').hide();
                $('.not-authenticated').show();
                $("#login-link").click();
            }
        }
    });

    /* Modal events */

    var $mask = $("#mask");
    var $window_class = $(".window");

    $("a.modal").click(function(e) {
        e.preventDefault();

        var id = $(this).attr("href");

        function success(modal_content) {
            var $window = $(id);

            $window.find('.modal_content').html(modal_content);

            //Get the screen height and width
            var maskHeight = $(document).height();
            var maskWidth = $(window).width();
            var $input = $window.find('.modal_content input');

            //Set height and width to mask to fill up the whole screen
            $mask.css({width: maskWidth, height: maskHeight});

            //transition effect
            $mask.fadeTo("fast", 0.7);

            Chat.modal.centerModal($window_class);

            //transition effect
            $window.fadeIn(500);

            // cursor into first form field, if present
            if ($input.length > 0) {
                $input.first().focus();
            }
        }

        if (id == '#register') {
            Chat.register(success);
        } else {
            Chat.login(success);
        }
    });

    $(".window .close").click(function (e) {
        e.preventDefault();
        $mask.hide();
        $window_class.hide();
    });

    $(window).on('resize', function(e) {
        $mask.css({width: $(document).width(), height: $(document).height()});
        Chat.modal.centerModal($window_class);
    });

    // escape key closes modal
    $(document).keyup(function(e) {
        if (e.keyCode === 27) {
            $mask.hide();
            $window_class.hide();
        }
    });

    // modal form submissions
    $(document).on('submit', '#login form', function(e) {
        e.preventDefault();
        Chat.loginSubmit($(this));
    });

    $(document).on('submit', '#register form', function(e) {
        e.preventDefault();
        Chat.registerSubmit($(this));
    });

    $('#logout-link').click(function(e) {
        e.preventDefault();
        Chat.logoutSubmit();
    });
};

/**
 * Retrieves messages from the server for the current user.
 * @param count
 */
Chat.getMessages = function(count)
{
    if (!count) {
        count = 5;
    }
    $.ajax({
        type: 'get',
        url: '/messages/',
        data: {
            count: count
        },
        dataType: 'json',
        success: Chat.displayMessage,
        error: function() {
            Chat.errorHandler("Unable to retrieve messages from the server, there was an error.");
        }
    });
};

/**
 *
 * Displays messages on the screen (if they are not already displayed)
 * @param data the data from the JSON callback
 */
Chat.displayMessage = function(data)
{
    var $messages = $('#messages');
    // if we got here then the user must be authenticated
    $('.authenticated').show();
    $('.not-authenticated').hide();
    // messages have most recent at the bottom:
    data.reverse();
    for (var i = 0; i < data.length; i++) {
        if (Chat.received_message_ids.indexOf(data[i].message_id) == -1) {
            Chat.processTemplate('message', $messages, data[i]);
            Chat.received_message_ids.push(data[i].message_id);
        }
    }
    Chat.markMessagesRead(Chat.received_message_ids);
};

/**
 * Posts a message to the server
 * @param message
 */
Chat.postMessage = function(message)
{
    message = $.trim(message);
    if (message.length == 0) {
        Chat.errorHandler("No message posted, there was no message to post!");
    } else {
        $.ajax({
            type: 'post',
            url: '/messages/',
            dataType: 'json',
            data: {
                message: message
            },
            success: function(data) {
                if (data.message_id) {
                    Chat.processTemplate('message', $('#messages'), data);
                    Chat.markMessagesRead([data.message_id]);
                    Chat.received_message_ids.push(data.message_id);
                    $('#message_text').focus().val("");
                } else {
                    Chat.errorHandler("Message posted, but received an unexpected response from the server.");
                }
            },
            error: function() {
                Chat.errorHandler("Unable to post the message, an error was returned from the server.");
            }
        });
    }
};

/**
 * Retrieves a named template via AJAX (if it has not already been loaded) and calls the callback when successful.
 * Uses the underscore.js engine to compile the templates
 * @param name
 * @param $destination
 * @param params
 */
Chat.processTemplate = function(name, $destination, params, callback)
{
    if (typeof Chat.templates[name] != 'undefined') {
        var template_result = Chat.templates[name](params);
        if (callback) {
            callback(template_result);
        } else {
            $destination.append(template_result);
        }
    } else {
        $.ajax({
            type: 'get',
            url: '/template/' + encodeURIComponent(name),
            dataType: 'text',
            async: false, // this can be done because processTemplate is called in a background process anyway.
            success: function(template) {
                Chat.templates[name] = _.template(template);
                var template_result = Chat.templates[name](params);
                if (callback) {
                    callback(template_result);
                } else {
                    $destination.append(template_result);
                }
            },
            error: function () {
                alert("Unable to retrieve template: " + name);
            }
        });
    }
};

/**
 * Once a set of messages has been displayed, they are considered read. This lets the server know.
 * @param message_ids
 */
Chat.markMessagesRead = function(message_ids)
{
    if (message_ids.length > 0) {
        Chat.scrollToLastMessage(message_ids[message_ids.length - 1]);
        message_ids = message_ids.join(",");
        $.ajax({
            type: 'post',
            url: '/message-read/',
            data: {
                message_ids: message_ids
            },
            success: function() {} // do nothing
        });
    }
};

Chat.scrollToLastMessage = function(id)
{
    var $messages = $('#messages');
    var $last_child = $messages.find(':last-child');
    if ($last_child.length > 0) {
        $messages.stop().animate({
            scrollTop: $messages[0].scrollHeight
        }, 800);
    }
};

/**
 * Displays errors to the user and hides them again after Chat.error_hide_timeout ms
 * @param error_message
 */
Chat.errorHandler = function(error_message)
{
    var $dest = $('#errors');
    Chat.processTemplate('error', $dest, {message: error_message});
    $dest.show();
    // after timeout, hide the error
    setTimeout(function() {
        $dest.find('.error:visible').last().hide();
        if ($dest.find('.error:visible').length == 0) {
            $dest.hide();
            $dest.html("");
        }
    }, Chat.error_hide_timeout);
};

Chat.login = function(success_callback)
{
    Chat.processTemplate('login', $('#login'), [], success_callback);
};

Chat.loginSubmit = function($form_element)
{
    $.ajax({
        url: $form_element.attr('action'),
        type: 'post',
        dataType: 'json',
        data: {
            username: $form_element.find('#username').val(),
            password: $form_element.find('#password').val()
        },
        success: function(data) {
            $form_element.closest('.window').find('.close').click();
        }
    });
};

Chat.logoutSubmit = function()
{
    $.ajax({
        url: "/logout/",
        type: "get",
        success: function(data) {
            $('.authenticated').hide();
            $('.not-authenticated').show();
        }
    });
};

Chat.register = function(success_callback)
{
    Chat.processTemplate('register', $('#register'), [], success_callback);
};

Chat.registerSubmit = function($form_element)
{

};

/* Modal */

Chat.modal = function(template_name, $content_elem, params)
{

};

Chat.modal.centerModal = function ($window)
{
    // Get the window height and width
    var winH = $(window).height();
    var winW = $(window).width();

    // Set the window to center
    $window.css("top",  winH / 2 - $window.height() / 2);
    $window.css("left", winW / 2 - $window.width() / 2);
};

Chat.modal.windowResizeCallback = function ()
{
    var $box = $("#boxes .window");

    //Get the screen height and width
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();

    //Set height and width to mask to fill up the whole screen
    $mask.css({width: maskWidth, height: maskHeight});

    Chat.modal.centerModal($box);
};
