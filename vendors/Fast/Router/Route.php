<?php

namespace Fast\Router;

use Fast\App;
use Fast\Request\Request;

class Route {

    const TYPE_MATCH = 'MATCH';
    const TYPE_DYNAMIC = 'DYNAMIC';
    const TYPE_REGEX = 'REGEX';

    /**
     * Available types
     * @var string[string]
     */
    protected static $types = [
        'TYPE_MATCH' => self::TYPE_MATCH,
        'TYPE_DYNAMIC' => self::TYPE_DYNAMIC,
        'TYPE_REGEX' => self::TYPE_REGEX
    ];

    /**
     * @var null|string
     */
    protected $controller = '';

    /**
     * @var string
     */
    protected $route = '';

    /**
     * @var string
     */
    protected $name = '';

    /**
     * The type of matching for the route (one of the the TYPE_* class constants)
     * @var string
     */
    protected $type = self::TYPE_MATCH;

    public function __construct($name, $route, $controller=null)
    {
        $this->setName($name);
        $this->setRoute($route);
        $this->setController($controller);
    }

    /**
     * @param string $name
     * @return static
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets available type of routes as array, related to the TYPE_* class constants
     * @return string
     */
    public static function getTypes()
    {
        return static::$types;
    }

    /**
     * @param string $type one of the TYPE_* class constants
     * @throws Exception
     * @return static
     */
    public function setType($type)
    {
        if (!in_array($type, self::$types)) {
            throw new Exception('Unknown type: ' . $type);
        }
        $this->type = $type;
        return $this;
    }

    /**
     * @return string one of the TYPE_* class constants
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $route
     */
    public function setRoute($route)
    {
        if ($route == '/') {
            $this->route = '/';
        } else {
            $this->route = rtrim($route, '/');
        }
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $uri
     * @param Request $request
     * @return bool
     */
    public function isMatch($uri, Request $request)
    {
        $is_match = false;
        switch ($this->getType()) {
            case self::TYPE_MATCH :
                if (trim($uri, '/') == trim($this->route, '/')) {
                    $is_match = true;
                }
                break;
            case self::TYPE_REGEX :
                $reg_ex = $this->route;
                preg_match_all('/' . $reg_ex . '/i', $uri, $matches);
                break;
            case self::TYPE_DYNAMIC :
                $reg_ex = '{^' . preg_replace('/:([^\/]+)/', '(?<$1>[^\/]+)', $this->route) . '(?=\/$|$)}';
                preg_match_all($reg_ex, $uri, $matches);
                break;
        }
        if (isset($matches)) {
            foreach ($matches as $key => $match) {
                if (is_numeric($key)) {
                    unset($matches[$key]);
                    continue;
                }
                if ($match) {
                    $is_match = true;
                    if (is_array($match)) {
                        $request->get()->set($key, $match[0]);
                    } else {
                        $request->get()->set($key, $match);
                    }

                }
            }
        }
        return $is_match;
    }

    /**
     * Generates a URL based on this route. If params are supplied and are not needed
     * for the current route, they are added as GET parameters.
     * @param string[] $params
     * @return string
     * @throws Exception
     */
    public function generateURL($params=[])
    {
        $qs = '';
        if ($this->isDynamic()) {
            if (count($params) == 0) {
                throw new Exception("To generate a dynamic route, parameters must be passed to generateURL() for the route: " . $this->getName());
            }
            $url = $this->getRoute();
            foreach ($params as $name => $value) {
                $url = str_replace(':' . $name, rawurlencode($value), $url);
                unset($params[$name]);
            }
            if ($params) {
                $qs = '?' . http_build_query($params);
            }
        } else {
            $url = $this->getRoute();
        }
        return $url . $qs;
    }
} 