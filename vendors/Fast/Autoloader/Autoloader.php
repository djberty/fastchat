<?php

namespace Fast\Autoloader;

use Fast\View\Exception;

class Autoloader {

    /**
     * @var static
     */
    protected static $instance = null;

    /**
     * @return static
     */
    public static function instance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * Initialises the autoloader
     */
    public static function init()
    {
        $instance = static::instance();
        spl_autoload_register(array($instance, 'load'));
    }

    /**
     * The method assigned to spl_autoload_register()
     * @param string $classname
     * @throws Exception
     */
    public function load($classname)
    {
        $this->requirePHPFile($classname);
    }

    /**
     * @param string $filename
     * @param string $prefix The path to prefix onto the generated path
     * @throws Exception
     * @return bool
     */
    public function requirePHPFile($filename, $prefix='vendors/')
    {
        $path = str_replace('\\', '/', $filename) . '.php';
        if (!is_file(FAST_ROOT . $prefix . $path)) {
            throw new Exception("Filename not found: " . $filename . " (prefix: " . $prefix . ")");
        }
        require_once FAST_ROOT . $prefix . $path;
        return true;
    }

    /**
     * @param string $name
     * @throws Exception
     * @return bool
     */
    public function loadController($name)
    {
        return $this->requirePHPFile($name, 'app/Controllers/');
    }

    /**
     * @param string $name
     * @throws Exception
     * @return bool
     */
    public function loadModel($name)
    {
        return $this->requirePHPFile($name, 'app/Models/');
    }

    /**
     * Reads and returns the contents of a resource from the Application's Resource directory
     * @param string $name
     * @return string
     */
    public function loadResource($name)
    {
        $path = FAST_ROOT . 'app/Resources/' . $name;
        if (is_file($path) && is_readable($path)) {
            return file_get_contents($path);
        }
        return null;
    }
} 