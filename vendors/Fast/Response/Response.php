<?php

namespace Fast\Response;

use Fast\Config\Config;

class Response {

    /**
     * The body of the response
     * @var string
     */
    protected $body = '';

    /**
     * An array of response headers
     * @var string[]
     */
    protected $headers = [];

    /**
     * The response code
     * @var int
     */
    protected $status = 200;

    public function __construct(Config $config = null)
    {
        if ($config) {
            // @todo Config
        }
    }

    /**
     * @param string $body
     * @return static
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * Sets the HTTP status Code
     * @param int $status
     * @return static
     */
    public function setStatus($status)
    {
        $this->status = (int) $status;
        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return static
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
        return $this;
    }

    /**
     * Renders the response
     */
    public function render()
    {
        foreach ($this->headers as $name => $value) {
            header($name . ': ' . $value, true, $this->status);
        }
        http_response_code($this->status);
        echo $this->body;
    }
} 