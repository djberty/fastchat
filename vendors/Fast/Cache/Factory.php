<?php

namespace Fast\Cache;

use Fast\Config\Config;

class Factory
{
    const TYPE_APC = 'APC';
    const TYPE_FILE = 'File';

    /**
     * @param Config $config
     * @return Cache
     * @throws Exception
     */
    public static function fromConfig(Config $config = null)
    {
        if (isset($config->type)) {
            $type = $config->type;
            if (!defined('self::' . strtoupper($type))) {
                throw new Exception("Unknown cache type: " . $type);
            }
        } else {
            $type = self::TYPE_APC;
        }

        if ($type == self::TYPE_FILE) {
            if (!isset($config->path)) {
                throw new Exception('File cache requires a "path" config value. None found.');
            } else {
                return new Adaptor\File($config->path);
            }
        }

        $class = '\\Fast\\Cache\\Adaptor\\' . $type;
        if (!class_exists($class)) {
            throw new Exception('Could not create cache class: ' . $class);
        }
        return new $class();
    }
} 