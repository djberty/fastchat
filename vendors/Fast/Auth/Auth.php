<?php

namespace Fast\Auth;

use Fast\Config\Config;
use Fast\Request\Session;

class Auth
{
    /**
     * @var Model
     */
    protected $model = null;

    /**
     * @var Session
     */
    protected $session = null;

    /**
     * @param Session $session
     * @param Config $config (Optional)
     */
    public function __construct(Session $session, Config $config = null)
    {
        if ($config) {
            // @todo Auth config
        }
        $this->session = $session;
    }

    /**
     * @param Model $model
     * @return static
     */
    public function setAuthModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Determines if the passed in password is valid for the user denoted by the username in the model
     * @param string $password
     * @throws Exception
     * @return bool
     */
    public function authenticate($password)
    {
        try {
            $credentials = $this->model->getCredentials();
            if ($this->hashPassword($password, $credentials->getSalt()) == $credentials->getPassword()) {
                $this->session->set('user_id', $credentials->getUserId());
                return true;
            }
        } catch (Exception $e) {
            throw new Exception("Could not load user's credentials", 500, $e);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        return (bool) $this->session->get('user_id');
    }

    /**
     * Logs a user out
     */
    public function unauthenticate()
    {
        $this->session->remove('user_id');
    }

    /**
     * Hashes password (with salt) using passed in algorithm
     * @param string $password
     * @param string $salt
     * @param string $algorithm (default: sha256)
     * @return string
     */
    public function hashPassword($password, $salt, $algorithm='sha256')
    {
        return hash($algorithm, $password . $salt);
    }

    /**
     * Generates a random alpha numeric string of $length
     * @param int $length (defaults to 20)
     * @return string
     */
    public function generateSalt($length=20)
    {
        $candidates = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!^$*()';
        $candidates_length = strlen($candidates);
        $output = '';
        while (strlen($output) < $length) {
            $char = $candidates[mt_rand(0, $candidates_length - 1)];
            if (strpos($output, $char) !== false) {
                continue; // we already have that character in the string
            } else {
                $output .= $char;
            }
        }
        return $output;
    }
} 