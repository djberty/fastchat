<?php

namespace Fast\Auth;

class Credentials
{
    /**
     * The identifier for the user, if they are authenticated
     * @var null | mixed
     */
    protected $user_id = null;

    /**
     * @var string
     */
    protected $username = '';

    /**
     * @var string
     */
    protected $password = '';

    /**
     * @var string
     */
    protected $salt = '';

    /**
     * @param string $username
     * @param mixed $user_id
     * @param string $password
     * @param string $salt
     */
    public function __construct($username = null, $user_id = null, $password = null, $salt = null)
    {
        $this->setUsername($username);
        $this->setUserId($user_id);
        $this->setPassword($password);
        $this->setSalt($salt);
    }

    /**
     * @param mixed|null $user_id
     * @return static
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param string $password
     * @return static
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $salt
     * @return static
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $username
     * @return static
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }


} 