<?php

namespace Fast\Model;

use Fast\DB\DB;

class MySQL extends Model
{
    /**
     * @var DB
     */
    protected $db = null;

    /**
     * @param DB $db
     */
    public function setDB(DB $db)
    {
        $this->db = $db;
    }

    /**
     * @return DB
     * @throws Exception
     */
    protected function getDB()
    {
        if (!$this->db instanceof DB) {
            throw new Exception("Cannot use a MySQL Model until the database instance has been passed in using setDB()");
        }
        return $this->db;
    }

    /**
     * Loads the current row data (and must be set via setData($data))
     * @return void
     */
    protected function loadRow()
    {
        $sql = 'SELECT `' . implode('`,`', $this->fields) . '`
                FROM `' . $this->table . '`
                WHERE `' . $this->primary_key . '` = :primary_key
                LIMIT 1';

        $query = $this->getDB()->prepare($sql);
        if ($query) {
            if ($query->execute([':primary_key' => $this->getId()])) {
                $row = $query->fetch();
                if ($row) {
                    $this->setData($row);
                }
            }
        }
    }

    /**
     * Saves the current row data
     * @param string[] $save_data an array of key values containing the fields that have changed and need saving
     * @param string $save_type one of the Model::SAVE_TYPE_* constants
     * @return bool
     * @throws Exception
     */
    protected function saveRow(array $save_data, $save_type)
    {
        foreach ($save_data as $key => $value) {
            $data[':' . $key] = $value;
        }
        if (!$data) {
            return false;
        }

        if ($save_type == Model::SAVE_TYPE_INSERT) {
            $fields = $save_data;
            unset($data[':primary_key'], $fields['primary_key']);
            $sql = 'INSERT INTO `' . $this->table . '`
                        (`' . implode('`, `', array_keys($fields)) . '`)
                    VALUES
                        (' . implode(', ', array_keys($data)) . ')';
        } elseif ($save_type == Model::SAVE_TYPE_UPDATE) {
            $sql = 'UPDATE `' . $this->table . '`
                    SET ';
            foreach ($save_data as $key => $value) {
                $sql .= ($key . ' = :' . $key . ',');
            }
            $sql = substr($sql, 0, -1); // remove last ,
            $sql .= ' WHERE `' . $this->primary_key . '`= :' . $this->primary_key . '
                      LIMIT 1';
        } else {
            throw new Exception("Unknown save_type: " . $save_type);
        }

        $query = $this->getDB()->prepare($sql);
        if ($query) {
            return $query->execute($data);
        }
        return false;
    }

    /**
     * Deletes the current row data
     * @return bool
     */
    protected function deleteRow()
    {
        $sql = 'DELETE FROM `' . $this->table . '`
                WHERE `' . $this->primary_key . '` = :primary_key
                LIMIT 1';

        $query = $this->getDB()->prepare($sql);
        if ($query) {
            return $query->execute([':primary_key' => $this->getId()]);
        }
        return false;
    }

    /**
     * @return mixed
     */
    protected function getLastInsertId()
    {
        return $this->getDB()->lastInsertId();
    }
} 