<?php

namespace Fast\Model;

abstract class Model
{
    const SAVE_TYPE_INSERT = 'INSERT';
    const SAVE_TYPE_UPDATE = 'UPDATE';

    /**
     * Must be set in concrete classes, defining the table name
     * @var string
     */
    protected $table = '';

    /**
     * Must be set in concrete classes, defining field names
     * @var string[]
     */
    protected $fields = [];

    /**
     * Must be set in concrete classes, defining the name of the primary key for the table
     * @var string
     */
    protected $primary_key = '';

    /**
     * The loaded data for the current row
     * @var string[]
     */
    protected $data = [];

    /**
     * Whether the data has been loaded
     * @var bool
     */
    protected $loaded = false;

    /**
     * @param string[] $data (Optional)
     * @throws Exception
     */
    public function __construct(array $data = [])
    {
        // populate $this->data from passed in array based on contents of $this->columns
        if ($data) {
            $this->setData($data);
        }
    }

    /**
     * Called before any database operations to ensure we have the necessary fields in the concrete class
     */
    protected function checkConcreteProperties()
    {
        // check we have a table name
        if (!isset($this->table) || !$this->table) {
            throw new Exception("A table name must be defined in the concrete class: " . get_class($this));
        }

        // check we have primary key
        if (!isset($this->primary_key) || !$this->primary_key) {
            throw new Exception("A primary_key must be defined in the concrete class: " . get_class($this));
        }
        if (!isset($this->fields[$this->primary_key]) || !$this->fields[$this->primary_key]) {
            // the primary key has not been defined in the $this->columns array, lets add it to the top
            array_unshift($this->fields, $this->primary_key);
        }

        // check we have a columns array
        if (!isset($this->fields) || !$this->fields) {
            throw new Exception("Fields must be defined in the fields array in the concrete class: " . get_class($this));
        }
    }

    /**
     * Sets a named field to the specified value
     * @param string $name
     * @param mixed $value
     * @return static
     * @throws Exception
     */
    public function setField($name, $value)
    {
        if (in_array($name, $this->fields)) {
            if ($value instanceof MySQLDate) {
                $value = $value->asMySQLDateTime();
            }
            $this->data[$name] = $value;
            return $this;
        }
        throw new Exception("Attempt to set unknown field: " . $name . " to value: " . $value);
    }

    /**
     * Retrieves the named field value
     * @param string $name
     * @return string
     * @throws Exception
     */
    public function getField($name)
    {
        if (!$this->loaded) {
            $this->load();
            $this->loaded = true;
        }
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        throw new Exception("Attempt to retrieve unknown field: " . $name);
    }

    /**
     * Gets the current value for the primary_key
     * @param mixed $id
     * @return static
     */
    public function setId($id)
    {
        $this->data[$this->primary_key] = $id;
        $this->loaded = false;
        return $this;
    }

    /**
     * Gets the current value for the primary_key
     * @return mixed
     */
    public function getId()
    {
        if (isset($this->data[$this->primary_key])) {
            return $this->data[$this->primary_key];
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getPrimaryKey()
    {
        return $this->primary_key;
    }

    /**
     * Array representation
     * @return string[]
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * Sets the data based on the keys in $this->columns
     * @param $data
     */
    public function setData($data)
    {
        foreach ($this->fields as $field) {
            if (isset($data[$field])) {
                $this->data[$field] = $data[$field];
            } else {
                $this->data[$field] = null;
            }
        }
        if (isset($this->data[$this->primary_key]) && $this->data[$this->primary_key] && count($this->data) == count($this->fields)) {
            $this->loaded = true;
        }
    }

    /**
     * Publicly accessible method to load row data
     * @param mixed $id (Optional - reverts to value for primary_key)
     * @return static
     * @throws Exception
     */
    public function load($id=null)
    {
        $this->checkConcreteProperties();
        if ($id === null) {
            if (!isset($this->data[$this->primary_key]) || !$this->data[$this->primary_key]) {
                throw new Exception("Cannot load data without a value present for the primary key");
            }
        } else {
            $this->data[$this->primary_key] = $id;
        }

        $this->loadRow();
        $this->loaded = true;

        return $this;
    }

    /**
     * Loads the current row data (and must be set via setData($data))
     * @return void
     */
    abstract protected function loadRow();

    /**
     * Publicly accessible method to save the row data
     * @return bool (always true, on failure - an exception will be raised)
     * @throws Exception
     */
    public function save()
    {
        $this->checkConcreteProperties();
        if (isset($this->data[$this->primary_key]) && $this->data[$this->primary_key]) {
            $save_type = static::SAVE_TYPE_UPDATE;
        } else {
            $save_type = static::SAVE_TYPE_INSERT;
        }
        if ($this->saveRow($this->data, $save_type)) {
            if ($save_type == static::SAVE_TYPE_INSERT) {
                $this->data[$this->primary_key] = $this->getLastInsertId();
            }
            $this->loaded = true;
            return true;
        } else {
            throw new Exception("Save failed, save_type: " . $save_type);
        }
    }

    /**
     * Saves the current row data
     * @param string[] $save_data an array of key values containing the fields that have changed and need saving
     * @param string $save_type one of the Model::SAVE_TYPE_* constants
     * @return bool
     */
    abstract protected function saveRow(array $save_data, $save_type);

    /**
     * Publicly accessible method to delete the row data
     * @throws Exception
     * @return bool
     */
    public function delete()
    {
        $this->checkConcreteProperties();
        if (!isset($this->data[$this->primary_key]) || !$this->data[$this->primary_key]) {
            throw new Exception("Cannot delete row data without a primary_key field value set.");
        }
        if ($this->deleteRow()) {
            $this->loaded = false;
            $this->data = [];
            return true;
        }
        return false;
    }

    /**
     * Deletes the current row data
     * @return bool
     */
    abstract protected function deleteRow();

    /**
     * @return mixed
     */
    abstract protected function getLastInsertId();
} 