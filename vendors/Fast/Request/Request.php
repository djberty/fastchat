<?php

namespace Fast\Request;

use Fast\Config\Config;
use Fast\Set;

class Request
{
    protected $method = '';
    protected $protocol = '';
    protected $port = 0;
    protected $host = '';
    protected $path = '';
    protected $query_string = '';

    protected $headers = [];
    protected $accept_content_types = [];

    /**
     * @var Cookies
     */
    protected $cookies = null;

    /**
     * @var Session[]
     */
    protected $sessions = [];

    /**
     * @var Set
     */
    protected $get = null;

    /**
     * @var Set
     */
    protected $post = null;

    /**
     * @param Config $config
     * @param bool $init Whether to auto-load from HTTP Request (default: true)
     */
    public function __construct(Config $config = null, $init = true)
    {
        if ($config) {
            // @todo Config
        }

        if ($init) {
            $this->initFromHTTP();
        }
    }

    /**
     * Initialises this request from the standard HTTP request
     */
    public function initFromHTTP()
    {
        $this->method = strtoupper($_SERVER['REQUEST_METHOD']);
        $this->protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']) ? 'https' : 'http';
        $this->port = (int) $_SERVER['SERVER_PORT'];
        $this->host = $_SERVER['HTTP_HOST'];
        $this->query_string = $_SERVER['QUERY_STRING'];

        if (strpos($_SERVER['REQUEST_URI'], '?') !== false) {
            $this->path = explode('?', $_SERVER['REQUEST_URI'])[0];
        } else {
            $this->path = $_SERVER['REQUEST_URI'];
        }

        $this->headers = apache_request_headers();

        if (isset($this->headers['Accept'])) {
            $accept = explode(',', $this->headers['Accept']);
            foreach ($accept as $accept_header) {
                $this->accept_content_types[] = explode(';', $accept_header)[0];
            }
        }
    }

    /**
     * @return string[]
     */
    public function getAcceptedContentTypes()
    {
        return $this->accept_content_types;
    }

    /**
     * @param string $host
     * @return static
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $method
     * @return static
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $path
     * @return static
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param int $port
     * @return static
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $protocol
     * @return static
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
        return $this;
    }

    /**
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @param string $query_string
     * @return static
     */
    public function setQueryString($query_string)
    {
        $this->query_string = $query_string;
        return $this;
    }

    /**
     * @return string
     */
    public function getQueryString()
    {
        return $this->query_string;
    }

    /**
     * @return Set
     */
    public function post()
    {
        if ($this->post === null) {
            $this->post = new Set($_POST, 'POST');
            unset($_POST);
        }
        return $this->post;
    }

    /**
     * @return Set
     */
    public function get()
    {
        if ($this->get === null) {
            $this->get = new Set($_GET, 'GET');
            unset($_GET);
        }
        return $this->get;
    }

    /**
     * @param string $namespace
     * @return Session
     */
    public function session($namespace)
    {
        if (!isset($this->sessions[$namespace])) {
            $this->sessions[$namespace] = new Session($namespace);
        }
        return $this->sessions[$namespace];
    }

    /**
     * @return Cookies
     */
    public function cookies()
    {
        if ($this->cookies === null) {
            $this->cookies = new Cookies();
        }
        return $this->cookies;
    }

} 