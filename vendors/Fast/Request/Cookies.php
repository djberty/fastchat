<?php

namespace Fast\Request;

use Fast\Set;

class Cookies extends Set
{
    public function get($name)
    {
        $val = null;
        if (isset($_COOKIE[$name])) {
            $val = $_COOKIE[$name];
        }
        return $val;
    }

    /**
     * @param string $name
     * @param string $value
     * @param int | null $expire
     * @param string | null $path
     * @param string | null $domain
     * @param bool | null $secure
     * @param bool | null $httponly
     * @return static
     */
    public function set($name, $value, $expire=null, $path=null, $domain=null, $secure=null, $httponly=null)
    {
        setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
        parent::set($name, $value);
        return $this;
    }
} 