<?php

namespace Fast\Request;

use Fast\Set;

class Session extends Set
{
    /**
     * Pass in the $_SESSION array
     * @param string $namespace
     */
    public function __construct($namespace)
    {
        if (!static::getSessionId()) {
            session_start();
        }
        if (!isset($_SESSION[$namespace])) {
            $_SESSION[$namespace] = [];
        }
        parent::__construct([], $namespace);
        $this->data = &$_SESSION[$namespace];
    }

    /**
     * @return string
     */
    public static function getSessionId()
    {
        return session_id();
    }
} 