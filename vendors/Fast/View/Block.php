<?php

namespace Fast\View;

class Block
{
    /**
     * @var string
     */
    protected $content = '';

    /**
     * Sets the content for the block (over-writing existing content)
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Adds content to the block
     * @param string $content
     */
    public function appendContent($content)
    {
        $this->content .= $content;
    }

    /**
     * Marks the start of a captured area of a view
     */
    public function start()
    {
        ob_start();
    }

    /**
     * Marks the end of a captured area of a view (and records the result)
     */
    public function end()
    {
        $this->appendContent(ob_get_clean());
    }

    /**
     * Returns the content in it's current state
     * @return string
     */
    public function output()
    {
        return $this->content;
    }
} 